import { Component, OnInit, OnDestroy } from '@angular/core';
import { BillService } from '../shared/services/bill.service';

import { Bill } from '../shared/models/bill.model';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'wfm-bill-page',
  templateUrl: './bill-page.component.html',
  styleUrls: ['./bill-page.component.scss']
})
export class BillPageComponent implements OnInit, OnDestroy {

  subscription: Subscription;
  subscription1: Subscription;
  subscription2: Subscription;

  currency: any;
  bill: Bill;
  isLoaded: boolean = false;

  constructor(private billService: BillService) { }

  ngOnInit() {
    this.subscription1 = Observable.combineLatest(
      this.billService.getBill(),
      this.billService.getCurrency()
    ).subscribe((data: [Bill, any]) => {
      this.bill = data[0];
      this.currency = data[1];
      this.isLoaded = true;
    });
  }

  ngOnDestroy() {
    this.subscription1.unsubscribe();
    if(this.subscription2)
    {
      this.subscription2.unsubscribe();
    }
  }

  OnRefresh()
  {
    this.isLoaded = false;
    this.subscription2 = this.billService.getCurrency().subscribe((currency: any) => {
      this.currency = currency;
      this.isLoaded = true;
    });
  }

}
