import { Component, OnInit, OnDestroy } from '@angular/core';
import { BillService } from '../shared/services/bill.service';
import { CategoriesService } from '../shared/services/categories.service';
import { EventsService } from '../shared/services/events.service';
import { Observable, Subscription } from 'rxjs';
import { Bill } from '../shared/models/bill.model';
import { Category } from '../shared/models/category.model';
import { WFMEvent } from '../shared/models/event.model';

@Component({
  selector: 'wfm-plannig-page',
  templateUrl: './plannig-page.component.html',
  styleUrls: ['./plannig-page.component.scss']
})
export class PlannigPageComponent implements OnInit, OnDestroy {

  isLoaded: boolean = false;
  bill: Bill;
  categories: Category[] = [];
  events: WFMEvent[] = []

  sub1: Subscription;
  constructor(
    private billService: BillService,
    private categoriesService: CategoriesService,
    private eventsService: EventsService
  ) { }

  ngOnInit(): void {
    this.sub1 = Observable.combineLatest(
      this.billService.getBill(),
      this.categoriesService.getCategories(),
      this.eventsService.getEvents()
    ).subscribe((data: [Bill, Category[], WFMEvent[]]) => {
      this.bill = data[0];
      this.categories = data[1];
      this.events = data[2];

      this.isLoaded = true;
    });
  }

  ngOnDestroy(): void {
    if(this.sub1) this.sub1.unsubscribe();
  }

  getCategoryCost(cat: Category): number
  {
    const catEvents = this.events.filter((event) => {
      return event.category === cat.id && event.type === 'outcome';
    });
    return catEvents.reduce((total, e) => {
      total += e.amount;
      return total;
    }, 0);
  }

  private getPercents(cat: Category): number
  {
    const percent = (100 * this.getCategoryCost(cat)) / cat.capacity;
    return percent > 100 ? 100 : percent;
  }

  getCategoryPercent(cat: Category): string
  {
    return this.getPercents(cat) + '%';
  }

  getColorClass(cat: Category): string
  {
    const percent = this.getPercents(cat);
    return percent < 60 ? 'success' : percent >= 100 ? 'danger' : 'warning';
  }

}
