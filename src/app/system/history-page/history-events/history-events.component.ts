import { Component, OnInit, Input } from '@angular/core';
import { Category } from '../../shared/models/category.model';
import { WFMEvent } from '../../shared/models/event.model';

@Component({
  selector: 'wfm-history-events',
  templateUrl: './history-events.component.html',
  styleUrls: ['./history-events.component.scss']
})
export class HistoryEventsComponent implements OnInit {

  @Input() categories: Category[] = [];
  @Input() events: WFMEvent[] = [];

  searchValue = '';
  searchPlaceholder = 'Сумма';
  searchField = 'amount';

  constructor() { }

  ngOnInit(): void {
    this.events.forEach(e => {
      e.catName = this.categories.find(c => c.id === e.category).name;
    });
  }

  changeCriteria(field: string)
  {
    const nameMaps = {
      'amount': ' Сумма',
      'date': 'Дата',
      'category': 'Категория',
      'type': 'Тип'
    };

    this.searchPlaceholder = nameMaps[field];
    this.searchField = field;
  }

}
