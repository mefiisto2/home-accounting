import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { EventsService } from '../../shared/services/events.service';
import { CategoriesService } from '../../shared/services/categories.service';
import { WFMEvent } from '../../shared/models/event.model';
import { Category } from '../../shared/models/category.model';
import { Subscription } from 'rxjs';
import { BillService } from '../../shared/services/bill.service';
import { Bill } from '../../shared/models/bill.model';

@Component({
  selector: 'wfm-history-detail',
  templateUrl: './history-detail.component.html',
  styleUrls: ['./history-detail.component.scss']
})
export class HistoryDetailComponent implements OnInit, OnDestroy {

  event: WFMEvent;
  category: Category;
  billCurrency: string;
  isLoaded: boolean = false;
  
  sub1: Subscription;

  constructor(
    private route: ActivatedRoute,
    private eventsService: EventsService,
    private categoriesService: CategoriesService,
    private billService: BillService
  ) { }

  ngOnInit(): void {
    this.sub1 = this.route.params
      .mergeMap((params: Params) => {
        return this.eventsService.getEventById(params['id']);
      })
      .mergeMap((event: WFMEvent) => {
        this.event = event;
        return this.categoriesService.getCategoryById(event.category);
      })
      .mergeMap((category: Category) => {
        this.category = category;
        return this.billService.getBill()
      })
      .subscribe((bill: Bill) => {
        this.billCurrency = bill.currency;
        this.isLoaded = true;
      });
      
  }

  ngOnDestroy() {
    this.sub1.unsubscribe();
  }

}
