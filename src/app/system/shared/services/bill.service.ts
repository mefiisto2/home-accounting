import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseApi } from 'src/app/shared/core/base-api';
import { Bill } from '../models/bill.model';

@Injectable()
export class BillService extends BaseApi {
    constructor(public http: HttpClient) {
        super(http);
    }

    getBill() {
        return this.get('bill');
    }

    getCurrency(base: string = 'EUR')
    {
        return this.http.get(`http://data.fixer.io/api/latest?access_key=9f394904cefc0ed8c69c44045d9db337&${base}`);
    }

    updateBill(bill: Bill)
    {
        return this.put('bill', bill);
    }

}