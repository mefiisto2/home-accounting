import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { UserService } from '../../shared/services/users.service';
import { User } from 'src/app/shared/models/user.model';
import { Message } from 'src/app/shared/models/message.model';
import { AuthService } from 'src/app/shared/services/auth.service';
import { ThrowStmt } from '@angular/compiler';
import { fadeStateTrigger } from 'src/app/shared/animations/fade.animation';

@Component({
  selector: 'wfm-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [fadeStateTrigger]
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  message: Message;

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() 
  {
    this.message = new Message("danger", "");

    this.route.queryParams.subscribe((params: Params) => {
      if(params['nowCanLogin'])
      {
        this.showMessage({
          text: 'Now just login.', 
          type: 'success'});
      }
      else if(params['accessDenied'])
      {
        this.showMessage({
          text: 'You need to login.', 
          type: 'warning'});
      }
    });

    this.form = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, [Validators.required, Validators.minLength(6)])
    });
  }

  private showMessage(message: Message)
  {
    this.message = message;

    window.setTimeout(() => {
      this.message.text = "";
    }, 5000);
  }

  onFormSubmit() {
    const formData = this.form.value;

    this.userService.getUserByEmail(formData.email)
      .subscribe((user: User) => {
        if(user)
        {
          if(user.password === formData.password)
          {
            this.message.text = "";
            window.localStorage.setItem('user', JSON.stringify(user));
            this.authService.login();
            this.router.navigate(['/system', 'bill']);
          }
          else
          {
            this.showMessage({
              text: 'Password is not valid',
              type: 'danger'
            });
          }
        }
        else
        {
          this.showMessage({
            text: 'User is not exist',
            type: 'danger'
          });
        }
      });
  }
}
