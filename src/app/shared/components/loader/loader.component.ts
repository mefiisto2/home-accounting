import { Component } from "@angular/core";

@Component({
    selector: 'wfm-lodaer',
    template: '<div class="loader-animator"></div>',
    styleUrls: ['./loader.component.scss']
})
export class LoaderComponent {

}