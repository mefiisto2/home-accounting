export class AuthService {
    private isUserAuthendificated = false;

    login()
    {
        this.isUserAuthendificated = true;
    }

    logout()
    {
        this.isUserAuthendificated = false;
        window.localStorage.clear();
    }

    isLoggedIn(): boolean
    {
        return this.isUserAuthendificated
    }
}