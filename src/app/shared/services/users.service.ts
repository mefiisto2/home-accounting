import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';

import {User} from '../models/user.model';
import { BaseApi } from '../core/base-api';

@Injectable()
export class UserService extends BaseApi {
    constructor(public http: HttpClient) {
        super(http);
    }

    getUserByEmail(email: string) {
        return this.get(`users?email=${email}`)
            .map((users: User[]) => users[0] ? users[0] : undefined);
    }

    createNewUser(user: User) {
        return this.http.post(`users`, user);
    }
}