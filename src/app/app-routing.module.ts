import { NgModule } from "@angular/core";
import { RouterModule, PreloadAllModules } from '@angular/router';
import { NotFoundComponent } from './shared/components/not-found/not-found.component';

const routes = [
    {path: '', redirectTo: 'login', pathMatch: 'full'},
    {path: 'system',  loadChildren: () => import('./system/system.module').then(m => m.SystemModule)},
    {path: '**', component: NotFoundComponent}  
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {
        preloadingStrategy: PreloadAllModules
    })],
    exports: [RouterModule]
})

export class AppRoutingModule {}